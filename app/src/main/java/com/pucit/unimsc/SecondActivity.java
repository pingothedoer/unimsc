package com.pucit.unimsc;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

//        Bundle extras = getIntent().getExtras();
//        if (extras != null) {
//            String mValue = extras.getString("some_data");
//            Toast.makeText(this, mValue, Toast.LENGTH_LONG).show();
//        }


        String mValue = getIntent().getStringExtra("some_data");
        Toast.makeText(this, mValue, Toast.LENGTH_LONG).show();

    }
}

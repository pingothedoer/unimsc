package com.pucit.unimsc;

/**
 * Created by M. Ali Ansari
 * on 20/03/2019.
 */
public interface MyClickEvent {

    void onEmailClick(String email);
}

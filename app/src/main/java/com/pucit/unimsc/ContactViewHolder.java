package com.pucit.unimsc;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by M. Ali Ansari
 * on 18/03/2019.
 */
public class ContactViewHolder extends RecyclerView.ViewHolder {

    TextView tvName;
    TextView tvPhone;
    ImageButton btnCall;
    ImageButton btnMail;

    public ContactViewHolder(@NonNull View itemView) {
        super(itemView);
        tvName = itemView.findViewById(R.id.tv_name);
        tvPhone = itemView.findViewById(R.id.tv_phone);
        btnCall = itemView.findViewById(R.id.btn_call);
        btnMail = itemView.findViewById(R.id.btn_mail);
    }


}

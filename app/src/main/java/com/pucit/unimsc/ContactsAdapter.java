package com.pucit.unimsc;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by M. Ali Ansari
 * on 18/03/2019.
 */
public class ContactsAdapter extends RecyclerView.Adapter<ContactViewHolder> {


    private ArrayList<Contact> list = new ArrayList<Contact>();
    private Context mContext;
    private MyClickEvent listener;

    public ContactsAdapter(ArrayList<Contact> list, Context mContext, MyClickEvent listener) {
        this.list = list;
        this.mContext = mContext;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.contact_list_item, viewGroup, false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ContactViewHolder vh, int position) {

        final Contact mContact = list.get(position);

        vh.tvName.setText(mContact.getName());
        vh.tvPhone.setText(mContact.getPhone().getMobile());

        vh.btnMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               listener.onEmailClick(mContact.getEmail());

            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

package com.pucit.unimsc;

/**
 * Created by M. Ali Ansari
 * on 13/03/2019.
 */
public class Phone {

    String mobile;
    String home;
    String office;


    public Phone() {
    }

    public Phone(String mobile, String home, String office) {
        this.mobile = mobile;
        this.home = home;
        this.office = office;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }
}

package com.pucit.unimsc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by M. Ali Ansari
 * on 18/02/2019.
 */
public class HomeActivity extends AppCompatActivity implements MyClickEvent {


    private Button btn_get_contacts;
    private RecyclerView my_list_view;
    private String url = "https://api.androidhive.info/contacts/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btn_get_contacts = findViewById(R.id.btn_get_contacts);
        my_list_view = findViewById(R.id.my_list_view);

        btn_get_contacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                GetContactsTask task = new GetContactsTask(url);
                task.execute();

            }
        });

    }

    @Override
    public void onEmailClick(String email) {
        Intent intent = new Intent(Intent.ACTION_SEND);

        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        intent.putExtra(Intent.EXTRA_SUBJECT, "My Subject");
        intent.putExtra(Intent.EXTRA_TEXT, "My Message");
        intent.setType("message/rfc822");

        Intent chooser = Intent.createChooser(intent, "Select Your Email APP");
        startActivity(chooser);
    }


    private class GetContactsTask extends AsyncTask<Void, Void, Void> {


        private String mUrl;
        private ProgressDialog pDialog;
        private ArrayList<Contact> list = new ArrayList<Contact>();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(HomeActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        public GetContactsTask(String mUrl) {
            this.mUrl = mUrl;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            HttpHandler mHandler = new HttpHandler();
            String result = mHandler.makeServiceCall(mUrl);

            try {
                JSONObject mainResult = new JSONObject(result);
                JSONArray contactsList = mainResult.getJSONArray("contacts");

                for (int index = 0; index < contactsList.length(); index++) {
                    JSONObject jsonContact = contactsList.getJSONObject(index);


                    Contact mContact = new Contact();
                    mContact.setId(jsonContact.getString("id"));
                    mContact.setName(jsonContact.getString("name"));
                    mContact.setAddress(jsonContact.getString("address"));
                    mContact.setEmail(jsonContact.getString("email"));
                    mContact.setGender(jsonContact.getString("gender"));


                    JSONObject jsonPhone = jsonContact.getJSONObject("phone");
                    Phone mPhone = new Phone();
                    mPhone.setHome(jsonPhone.getString("home"));
                    mPhone.setMobile(jsonPhone.getString("mobile"));
                    mPhone.setOffice(jsonPhone.getString("office"));

                    mContact.setPhone(mPhone);

                    list.add(mContact);

                }

            } catch (JSONException e) {
                System.out.println("Something went wrong");
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
            }

            Toast.makeText(HomeActivity.this, list.size() + "", Toast.LENGTH_SHORT).show();


            ContactsAdapter mAdapter = new ContactsAdapter(list, HomeActivity.this , HomeActivity.this);
            RecyclerView.LayoutManager manager = new LinearLayoutManager(HomeActivity.this);
            my_list_view.setLayoutManager(manager);
            my_list_view.setAdapter(mAdapter);

        }
    }
}
